class Smpp:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def open(self, user, password):
        hidden_password = self._hide_password(password)
        print "Logged %s with password %s to %s:%s" % (user, hidden_password, self.host, self.port)

    def _hide_password(self, password):
        return '*' * len(password)

    def send(self, to, message):
        print "Sent message to %s" % to
        print message

    def close(self):
        print 'Connection close'