package com.taptapnetworks.codetest.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordUtilsTests {

	@Test
	public void testHidePassword() {
		
		PasswordUtils utils = new PasswordUtils();
		String out = utils.hidePassword("password");
		assertEquals("********", out);
		
		out = utils.hidePassword("");
		assertEquals("", out);
		
		out = utils.hidePassword("This is a very very long password");
		assertEquals("*********************************", out);
		
		out = utils.hidePassword("This is & num3r1c 1");
		assertEquals("*******************", out);
		
	}

}
