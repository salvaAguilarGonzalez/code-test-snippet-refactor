package com.taptapnetworks.codetest.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmsMessageTests {


	@Test
	public void testGetTag() {
//		This is an important test because we used tag as id for dynamic loading of senders
		Message message = new SmsMessage("to", "message");
		assertEquals(SmsMessage.tag, message.getTag());
	}

	@Test
	public void testSmsMessage() {
		Message message = new SmsMessage("to", "message");
		assertEquals("to", message.getTo());
		assertEquals("message", message.getMessage());
		
		message = new SmsMessage("", "message");
		assertEquals("", message.getTo());
		assertEquals("message", message.getMessage());
		
		message = new SmsMessage("to", "");
		assertEquals("to", message.getTo());
		assertEquals("", message.getMessage());
	}

}
