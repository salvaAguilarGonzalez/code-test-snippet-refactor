package com.taptapnetworks.codetest.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class MailMessageTests {

	@Test
	public void testGetTag() {
//		This is an important test because we used tag as id for dynamic loading of senders
		Message message = new MailMessage("to", "message", "subject");
		assertEquals(MailMessage.tag, message.getTag());
	}
	
	@Test
	public void testMailMessage() {
		Message message = new MailMessage("to", "message", "subject");
		assertEquals("to", message.getTo());
		assertEquals("message", message.getMessage());
		assertEquals("subject", ((MailMessage)message).getSubject());
		
		
		message = new MailMessage("", "message", "subject");
		assertEquals("", message.getTo());
		assertEquals("message", message.getMessage());
		assertEquals("subject", ((MailMessage)message).getSubject());
		
		message = new MailMessage("to", "", "subject");
		assertEquals("to", message.getTo());
		assertEquals("", message.getMessage());
		assertEquals("subject", ((MailMessage)message).getSubject());
		
		message = new MailMessage("to", "message", "");
		assertEquals("to", message.getTo());
		assertEquals("message", message.getMessage());
		assertEquals("", ((MailMessage)message).getSubject());
	}

}
