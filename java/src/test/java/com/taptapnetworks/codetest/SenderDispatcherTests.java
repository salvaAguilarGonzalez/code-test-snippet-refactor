package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.taptapnetworks.codetest.connectors.Sender;
import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.SmsMessage;
import com.taptapnetworks.codetest.exception.UnsupportedConnectorException;

public class SenderDispatcherTests {

	@Test
	public void testGetInstance() {
		SenderDispatcher dispatcher = SenderDispatcher.getInstance();
		assertNotNull(dispatcher);
		
	}


	@Test
	public void testGetEmptySenderForMessage() throws UnsupportedConnectorException {
		SenderDispatcher dispatcher = SenderDispatcher.getInstance();
		Sender sender = dispatcher.getEmptySenderForMessage(MailMessage.tag);
		
		assertNotNull(sender);
		
		sender = dispatcher.getEmptySenderForMessage(SmsMessage.tag);
		
		assertNotNull(sender);
	}
	

	@Test
	public void testCloseAll() {
		SenderDispatcher dispatcher = SenderDispatcher.getInstance();
		dispatcher.closeAll();
		
		assertTrue(dispatcher.getSendersLength()==0);
	}

}
