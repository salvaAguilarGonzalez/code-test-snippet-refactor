package com.taptapnetworks.codetest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.SmsMessage;

public class SnippetToRefactorTests {

	@Test
	public void testSend() {
		SnippetToRefactor snippet = new SnippetToRefactor();
		snippet.send(new MailMessage("to", "message", "subject"));
		//nothing to check here because of the return value of the method. In real world this method should return something different from void
		assertTrue(true);
		
		snippet.send(new SmsMessage("to", "message"));
		//nothing to check here because of the return value of the method. In real world this method should return something different from void
		assertTrue(true);

	}

}
