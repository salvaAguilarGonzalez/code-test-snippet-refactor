package com.taptapnetworks.codetest.connectors;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.SmsMessage;

public class SMPPTests {

	private SMPP conn;
	
	@Before
	public void setUp() throws Exception {
		conn = new SMPP("host", "port");
	}
	
	@Test
	public void testSMPP() {
		assertNotNull(conn);
		//we should check also the different values in creation of the object.
	}

	@Test
	public void testOpenConnection() {
		conn.openConnection("username", "password");
		//Nothing to test here because of the returning value of the method. In real world the result should not be void.
		assertTrue( true );
	}

	@Test
	public void testSend() {
		conn.openConnection("username", "password");//the execution order of the tests is not guaranteed.
		conn.send(new SmsMessage("to", "message"));
		//Nothing to test here because of the returning value of the method. In real world the result should not be void.
		assertTrue( true );
	}

	@Test
	public void testClose() {
		conn.openConnection("username", "password");//the execution order of the tests is not guaranteed.
		conn.close();
		//Nothing to test here because of the returning value of the method. In real world the result should not be void.
		assertTrue( true );

	}

}
