package com.taptapnetworks.codetest.connectors;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.taptapnetworks.codetest.entities.MailMessage;

public class SendMailConnectionTests {

	private SendMailConnection conn;
	
	@Before
	public void setUp() throws Exception {
		conn = new SendMailConnection("mailServer");
	}

	@Test
	public void testSendMailConnection() {
		assertNotNull(conn);
	}

	@Test
	public void testSend() {
		conn.send(new MailMessage("to", "message", "subject"));
		//Nothing to test here because of the returning value of the method. In real world the result should not be void.
		assertTrue( true );
	}

	@Test
	public void testClose() {
		conn.close();
		//Nothing to test here because of the returning value of the method. In real world the result should not be void.
		assertTrue( true );

	}

}
