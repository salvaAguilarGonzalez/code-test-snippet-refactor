package com.taptapnetworks;

import com.taptapnetworks.codetest.SnippetToRefactor;
import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.SmsMessage;

/**
 * Start class of the application
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        SnippetToRefactor sni = new SnippetToRefactor();
        sni.send(new MailMessage("to", "message", "subject"));
        sni.send(new SmsMessage("to", "message"));
    }
}
