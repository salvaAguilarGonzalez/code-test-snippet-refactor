package com.taptapnetworks.codetest.connectors;

import com.taptapnetworks.codetest.entities.Message;

/**
 * Sender Interface that is the parent to any new sender connector which could be added in the future to sending platform.
 * @author salva
 *
 */
public interface Sender {

	
	public void send(Message message);
	public void close();
}
