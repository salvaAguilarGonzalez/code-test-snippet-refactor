package com.taptapnetworks.codetest.connectors;

import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.Message;

/**
 * This class manages a single connection the connection with Mail server. Its parent is {@link Sender}
 * @author salva
 *
 */
public class SendMailConnection implements Sender{
	
	public String tag = "Mail_Sender";

    public SendMailConnection(String mailServer) {
        super();
        System.out.println(String.format("Created SendMailConnection for server %s", mailServer));
    }

    public void send(Message message) {
        System.out.println("Message sent!!!");
    }

    public void close() {
        System.out.println("Mail connection closed");
    }

}
