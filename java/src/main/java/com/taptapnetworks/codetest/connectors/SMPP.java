package com.taptapnetworks.codetest.connectors;

import com.taptapnetworks.codetest.entities.Message;
import com.taptapnetworks.codetest.entities.SmsMessage;
import com.taptapnetworks.codetest.utils.PasswordUtils;


/**
 * This class manages a single connection the connection with SMPP server. Its parent is {@link Sender}
 * @author salva
 *
 */
public class SMPP  implements Sender{
	public String tag = "SMPP_Sender";

	private final String host;
	private final String port;

	private boolean opened = false;


	public SMPP(String host, String port) {
		this.host = host;
		this.port = port;
	}

	public void openConnection(String userName, String password) {
		if(!opened){
			PasswordUtils passwordUtil = new PasswordUtils();
			String hiddenPassword = passwordUtil.hidePassword(password);
			System.out.println(String.format("Logged %s with password %s to %s:%s", userName, hiddenPassword, host, port));
			opened = true;
		}
	}

	public void send(Message message) {
		System.out.println(String.format("Sent message to %s", message.getTo()));
		System.out.println(message.getMessage());
	}

	public void close() {
		System.out.println("Connection closed");
		opened = false;
	}


}
