package com.taptapnetworks.codetest.exception;

/**
 * Parent Exception created for the connector management. It will be used in case of using new connectors 
 * that were not included correctly in the platform.
 * @author salva
 *
 */
public class UnsupportedConnectorException extends Exception {
	
	private static final long serialVersionUID = 1L;
	private String message;

	public UnsupportedConnectorException(String message){
		super();
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	
	
	

}
