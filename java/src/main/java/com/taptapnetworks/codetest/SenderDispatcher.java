package com.taptapnetworks.codetest;

import java.util.HashMap;

import com.taptapnetworks.codetest.connectors.SMPP;
import com.taptapnetworks.codetest.connectors.SendMailConnection;
import com.taptapnetworks.codetest.connectors.Sender;
import com.taptapnetworks.codetest.entities.MailMessage;
import com.taptapnetworks.codetest.entities.SmsMessage;
import com.taptapnetworks.codetest.exception.UnsupportedConnectorException;

/**
 * This class manages the available connections, making them opened and ready for being shared in all application
 * @author salva
 *
 */
public class SenderDispatcher {

	//we made a singleton class, because of there will be only one dispatcher for senders. 
	//In case of the application would need a pool of connections, 
	//this class must be reviewed for managing different threads.
	private static SenderDispatcher dispatcher;
	//This hashmap stores the connections in memmory to get access to them as quickly as possible.
	private HashMap<String, Sender> senders;
	
	private int sendersLength = 0;
	
	private SenderDispatcher(){
		senders = new HashMap<String, Sender>();
		registerAvailableSenders();
	}
	
	

	public static SenderDispatcher getInstance(){
		if(dispatcher == null){
			dispatcher = new SenderDispatcher();
		}
		return dispatcher;
	}
	
	
	private void registerAvailableSenders() {
	//Should take the information from configuration files, both information for open the connections and available connectors in app. 
	//For this example, we just dodge this point.
		
		//SMPP
		SMPP smpp = new SMPP("192.168.1.4", "4301");
		smpp.openConnection("your-username", "your-password");
		
		//for storing the relation between the sender and the message they manages, we used the tag field included in the 
		//specific message. This is a possible solution. Another one could be make it parametric with class name.
		senders.put(SmsMessage.tag, smpp);
		
		//Mail
		SendMailConnection smc = new SendMailConnection("mail.myserver.com");
		
		senders.put(MailMessage.tag, smc);
		
		
		sendersLength = senders.size();
	}
	
	public void closeAll(){
		for(Sender sender:senders.values()){
			sender.close();
		}
		senders.clear();
		
		sendersLength = senders.size();
	}
	
	public Sender getEmptySenderForMessage(String tag) throws UnsupportedConnectorException{
		//In this method we could manage the possible queue of senders
		Sender sender = senders.get(tag);
		if(sender == null){
			throw new UnsupportedConnectorException("Unsupported Connector");
		}
		return sender;
	}



	public int getSendersLength() {
		return sendersLength;
	}


	
	
}
