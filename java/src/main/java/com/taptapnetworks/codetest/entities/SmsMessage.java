package com.taptapnetworks.codetest.entities;

/**
 * This class represent the SMS Message that is going to be sent. It is a child class from {@link Message}.
 * @author salva
 *
 */
public class SmsMessage extends Message{

	// I have created this class, because probably the sms message will have more fields that could be appropiate to manage.
   public static  String tag = "SMS message";
	
   public SmsMessage(String to, String message){
	   super(to, message);
	   prepareMessage();
   }
	
   
   private void prepareMessage(){
	   System.out.println(String.format("Preparing SMS message for %s", this.getTo()));
       System.out.println(this.getMessage());
	}
   
   
   //Getters methods
   @Override
	public String getTag() {
		return tag;
	}
}
