package com.taptapnetworks.codetest.entities;

/**
 * This class represent the Mail Message that is going to be sent. It is a child class from {@link Message}.
 * @author salva
 *
 */
public class MailMessage extends Message{

	// I have created this class, because probably the sms message will have more fields that could be appropiate to manage.
	public static String tag = "Mail message";

	private String subject;

	public MailMessage(String to, String message, String subject){
		super(to, message);
		this.subject = subject;
		prepareMessage();
	}

	private void prepareMessage(){
		System.out.println(String.format("Preparing Mail message for %s", this.getTo()));
		System.out.println(String.format("Subject: %s", subject));
		System.out.println(this.getMessage());
	}

	
	//Getters/setters methods
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String getTag() {
		return tag;
	}




}
