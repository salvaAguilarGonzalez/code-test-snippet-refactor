package com.taptapnetworks.codetest.entities;

/**
 * Parent class of Message that represent any message model that could be added to the sending platform in a future.
 * @author salva
 *
 */
public abstract class Message {

	
	private String to;
	private String message;
	
	public Message(String to, String message){
		this.to = to;
		this.message = message;
	}
	
	
	//Getters/setters methods
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public abstract String getTag();
	
	
}
