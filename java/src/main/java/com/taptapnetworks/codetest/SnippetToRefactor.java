package com.taptapnetworks.codetest;

import com.taptapnetworks.codetest.connectors.SMPP;
import com.taptapnetworks.codetest.connectors.SendMailConnection;
import com.taptapnetworks.codetest.connectors.Sender;
import com.taptapnetworks.codetest.entities.Message;
import com.taptapnetworks.codetest.exception.UnsupportedConnectorException;


/**
 * This is the original class requested for refactoring. 
 * @author salva
 *
 */
public class SnippetToRefactor {
 
	/**
	 *I wanted to keep the functionality as original before refactoring code, so the method send perform as a wrapper 
	 *the sending of any {@link Message}. This could be the connection with any other part of the code which wants to
	 *send any message thorough the sending platform.
	 * @param message
	 */
	public void send(Message message) {

		SenderDispatcher senderDispatcher = SenderDispatcher.getInstance();
		Sender sender;
		try {
			sender = senderDispatcher.getEmptySenderForMessage(message.getTag());
			sender.send(message);
		} catch (UnsupportedConnectorException e) {
			System.out.println("Connector not found");
		}
	}
}
