package com.taptapnetworks.codetest.utils;


/**
 * This class includes all utility methods used in the password management.
 * @author salva
 *
 */
public class PasswordUtils {

	
	public String hidePassword(String password) {
    	StringBuilder sb = new StringBuilder();
        for(int i = 0; i < password.length(); i++) {
            sb.append("*");
        }
        return sb.toString();
    }
}
